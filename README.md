# Test assignment: multi-line chart with d3.js and Angular

## Overview

This project shows how to create a multi-line chart using D3.js and Angular. The chart displays company's financial indicators for July'18. The data is taken from the DB through API GET request.

## Get started

To run the project follow the steps below:

### Development mode

1. clone the repo git `clone https://bitbucket.org/olgakozhevnikova/d3-angular-project`
2. navigate to the folder `cd d3-angular-project`
3. install node modules `npm install`
4. run `ng serve` to start the app
5. navigate to `http://localhost:4200/` in your browser
