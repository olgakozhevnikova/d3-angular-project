import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import * as _ from 'lodash';

import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3ScaleChromatic from 'd3-scale-chromatic';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';

@Component({
	selector: 'app-chart',
	encapsulation: ViewEncapsulation.None,
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  topChartData = [];
  bottomChartData = [];
	data;
  svg: any;
  margin = {top: 20, right: 80, bottom: 30, left: 50};
  g: any;
  width: number;
  height: number;
  x;
  y1;
  y2;
  z;
  line;

  exData;

	constructor(
		private http: Http
	) {}

	ngOnInit() {
    this.getData();
  }

	getData() {
    return this.http.get('http://localhost:3000/data?fields=dm_ui_chart_1m_fact_value,dm_ui_chart_1m_plan_value,dm_ui_chart_1m_forecast_upd,dm_ui_chart_1m_forecast_24,dm_ui_chart_1m_forecast_3,dm_ui_chart_1m_ved,dm_ui_chart_1m_mae_24,dm_ui_chart_1m_mae_3&start_date_time=2018-07-01T00:00:00.267Z&end_date_time=2018-07-31T23:59:00.267Z')
    .subscribe(res => {
      const data = res.json();
			for (let i = 0; i < data.length; i++) {
        const keys = Object.keys(data[i]);
        for (let j = 0; j < keys.length; j++) {
          if (keys[j] === 'dm_ui_chart_1m_mae_24' || keys[j] === 'dm_ui_chart_1m_mae_3') {
            const index = _.findIndex(this.bottomChartData, {id: keys[j]});
            if (index > -1) {
              this.bottomChartData[index].values.push({ date: new Date(data[i].dttm), number: data[i][keys[j]] });
            } else if (keys[j] !== 'dttm') {
              const Formatdata = {
                id: keys[j],
                values: [{ date: new Date(data[i].dttm), number: data[i][keys[j]] }]
              };
              this.bottomChartData.push(Formatdata);
            }
          } else {
            const index = _.findIndex(this.topChartData, {id: keys[j]});
            if (index > -1) {
              this.topChartData[index].values.push({ date: new Date(data[i].dttm), number: data[i][keys[j]] });
            } else if (keys[j] !== 'dttm') {
              const Formatdata = {
                id: keys[j],
                values: [{ date: new Date(data[i].dttm), number: data[i][keys[j]] }]
              };
              this.topChartData.push(Formatdata);
            }
          }
        }
      }
      this.data = this.topChartData.map((v) => v.values.map((v) => v.date ))[0];
      this.initChart();
      this.drawAxis();
      this.drawPath();
    });
  }

	initChart() {
    this.svg = d3.select('svg');

    this.width = this.svg.attr('width') - this.margin.left - this.margin.right;
    this.height = this.svg.attr('height') - this.margin.top - this.margin.bottom;

    this.g = this.svg.append('g').attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    this.x = d3Scale.scaleTime().range([0, this.width]);
    this.y1 = d3Scale.scaleLinear().range([this.height - 200, 0]);
    this.y2 = d3Scale.scaleLinear().range([this.height, 0]);
    this.z = d3Scale.scaleOrdinal(d3ScaleChromatic.schemeCategory10);

    this.line = d3Shape.line()
      .curve(d3Shape.curveBasis)
      .x( (d: any) => this.x(d.date) )
      .y( (d: any) => this.y1(d.number) );

    this.x.domain(d3Array.extent(this.data.map((d) => +d)));

    this.y1.domain([
      d3Array.min(this.topChartData, function(c: any) { return d3Array.min(c.values, function(d: any) { return d.number; }); }),
      d3Array.max(this.topChartData, function(c: any) { return d3Array.max(c.values, function(d: any) { return d.number; }); })
    ]);

    this.y2.domain([
      d3Array.min(this.bottomChartData, function(c: any) { return d3Array.min(c.values, function(d: any) { return d.number; }); }),
      d3Array.max(this.bottomChartData, function(c: any) { return d3Array.max(c.values, function(d: any) { return d.number; }); })
    ]);

    this.z.domain(this.topChartData.map(function(c) { return c.id; }));
  }

	drawAxis() {
    this.g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + this.height + ')')
      .call(d3Axis.axisBottom(this.x));

    this.g.append('g')
      .attr('class', 'axis axis--y')
      .call(d3Axis.axisLeft(this.y1))
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '0.71em')
      .attr('fill', '#000')
      .text('Financial indicator');

    this.g.append('g')
      .attr('class', 'axis axis--y2')
      .call(d3Axis.axisLeft(this.y2))
      .append('text')
      .attr('translate', '50')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '0.71em')
      .attr('fill', '#000')
      .text('MAE');
  }

  drawPath() {
    const topChart = this.g.selectAll('.top')
      .data(this.topChartData)
      .enter().append('g')
      .attr('class', 'top');

    topChart.append('path')
      .attr('class', 'line')
      .attr('d', (d) => this.line(d.values) )
      .style('stroke', (d) => this.z(d.id) );

    topChart.append('text')
      .datum(function(d) { return {id: d.id, value: d.values[d.values.length - 1]}; })
      .attr('transform', (d) => 'translate(' + this.x(d.value.date) + ',' + this.y1(d.value.number) + ')' )
      .attr('x', 3)
      .attr('dy', '0.35em')
      .style('font', '10px sans-serif');
      // .text(function(d) { return d.id; });

    const bottomChart = this.g.selectAll('.bottom')
      .data(this.bottomChartData)
      .enter().append('g')
      .attr('class', 'bottom');

    bottomChart.append('path')
      .attr('class', 'line')
      .attr('d', (d) => this.line(d.values) )
      .style('stroke', (d) => this.z(d.id) );

    bottomChart.append('text')
      .datum(function(d) { return {id: d.id, value: d.values[d.values.length - 1]}; })
      .attr('transform', (d) => 'translate(' + this.x(d.value.date) + ',' + this.y2(d.value.number) + ')' )
      .attr('x', 3)
      .attr('dy', '0.35em')
      .style('font', '10px sans-serif');
      // .text(function(d) { return d.id; });
  }
}
